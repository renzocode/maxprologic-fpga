onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /bemicro_max10_top_vhd_tst/i1/SYS_CLK
add wave -noupdate /bemicro_max10_top_vhd_tst/i1/USER_LED
add wave -noupdate /bemicro_max10_top_vhd_tst/i1/divider_counter
add wave -noupdate /bemicro_max10_top_vhd_tst/i1/state
add wave -noupdate -expand /bemicro_max10_top_vhd_tst/i1/scanningLED
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {7350 ms}
