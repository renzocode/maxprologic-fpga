# maxprologic-fpga

## Download BeMicro10 files and Documentation

[altera BeMicro10](https://fpgacloud.intel.com/devstore/platform/?page=1&acds_version=any&family=max-10&board=4).

[BeMicro10 max 10 Kit Baseline Pinout](https://fpgacloud.intel.com/devstore/platform/15.1.0/Standard/bemicro-max-10-kit-baseline-pinout/)

