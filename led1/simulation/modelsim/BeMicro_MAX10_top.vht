-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "02/28/2021 23:09:14"
                                                            
-- Vhdl Test Bench template for design  :  BeMicro_MAX10_top
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY BeMicro_MAX10_top_vhd_tst IS
END BeMicro_MAX10_top_vhd_tst;
ARCHITECTURE BeMicro_MAX10_top_arch OF BeMicro_MAX10_top_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL SYS_CLK : STD_LOGIC;
SIGNAL USER_LED : STD_LOGIC_VECTOR(8 DOWNTO 1);
COMPONENT BeMicro_MAX10_top
	PORT (
	SYS_CLK : IN STD_LOGIC;
	USER_LED : OUT STD_LOGIC_VECTOR(8 DOWNTO 1)
	);
END COMPONENT;
BEGIN
	i1 : BeMicro_MAX10_top
	PORT MAP (
-- list connections between master ports and signals
	SYS_CLK => SYS_CLK,
	USER_LED => USER_LED
	);
init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                           
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        -- code executes for every event on sensitivity list  
WAIT;                                                        
END PROCESS always;                                          
END BeMicro_MAX10_top_arch;
