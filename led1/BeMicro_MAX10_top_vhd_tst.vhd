library ieee;
use ieee.std_logic_1164.all;

entity BeMicro_MAX10_top_vhd_tst is
end BeMicro_MAX10_top_vhd_tst;

architecture BeMicro_MAX10_top_arch of BeMicro_MAX10_top_vhd_tst is
	signal sys_clk: std_logic := '0';
	signal user_led: std_logic_vector(8 downto 1);
component BeMicro_MAX10_top
	port (
		sys_clk: in std_logic;
		user_led: out std_logic_vector(8 downto 1)
	);
end component;
begin
	i1: BeMicro_MAX10_top
	port map (
		sys_clk => sys_clk,
		user_led => user_led
	);
sys_clk <= not sys_clk after 10 ns;
init: process
begin
wait;
end process init;

always: process
begin
wait;
end process always;
end BeMicro_MAX10_top_arch;
	